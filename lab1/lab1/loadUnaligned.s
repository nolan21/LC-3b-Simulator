        .ORIG   X4000
        AND     R2, R1, x0001
        BRZ     ALIGNED
        LDB     R2, R1, #0 ; Load separate bytes
        LDB     R3, R1, #1
        LSHF    R3, R3, #8
        ADD     R0, R2, R3
        RET
ALIGNED LDW     R0, R1, #0 ; Already aligned
        RET
        .END